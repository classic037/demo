import logo from './logo.svg';
import './App.css';
const displayHeader = () => {
  let browserClass;
  var browserClass;
  if (
    (navigator.userAgent.indexOf("Opera") ||
      navigator.userAgent.indexOf("OPR")) != -1
  ) {
    browserClass = "opera";
  } else if (
    /edge|msie\s|trident\//i.test(window.navigator.userAgent) ||
    navigator.userAgent.indexOf("MSIE") != -1 ||
    !!document.documentMode === true
  ) {
    //IF IE > 10
    browserClass = "msie";
  } else if (navigator.userAgent.indexOf("Chrome") != -1) {
    browserClass = "chrome";
  } else if (navigator.userAgent.indexOf("Safari") != -1) {
    browserClass = "safari";
  } else if (navigator.userAgent.indexOf("Firefox") != -1) {
    browserClass = "firefox";
  }
  return browserClass;
}
function App() {
  console.log('test log');
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        {displayHeader()}
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
